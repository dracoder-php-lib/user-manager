<?php

namespace Dracoder\UserManager;

use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserManager
{
    private ObjectManager $entityManager;
    private UserPasswordEncoderInterface $passwordEncoder;
    private string $userClass;

    /**
     * UserManager constructor.
     *
     * @param ObjectManager $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param string $userClass
     */
    public function __construct(
        ObjectManager $entityManager,
        UserPasswordEncoderInterface $passwordEncoder,
        string $userClass
    ) {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
        $this->userClass = $userClass;
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return mixed
     */
    public function changePassword(string $email, string $password)
    {
        $user = $this->entityManager->getRepository($this->userClass)->findOneBy(['email' => $email]);
        if ($user) {
            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    $password
                )
            );
            $this->entityManager->flush();
        }

        return $user;
    }

    /**
     * @param string $email
     * @param string $password
     * @param array $roles
     *
     * @return mixed
     */
    public function createUser(string $email, string $password, array $roles = [])
    {
        $user = new $this->userClass;
        $user->setEmail($email);
        $user->setRoles($roles);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $password
            )
        );
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }
}
